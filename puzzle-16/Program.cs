﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace puzzle_16
{
  public class Packet
  {
    public int Version { get; set; }
    public int Type { get; set; }
  }
  
  public class LiteralPacket : Packet
  {
    public long Value { get; set; }

    public LiteralPacket(int version, int type, string valueInBinary)
    {
      Version = version;
      Type = type;
      int numberOfSkipsTotal = valueInBinary.Length/5;
      int totalDigitBits = valueInBinary.Length - numberOfSkipsTotal - 1;
      int numberOfSkips = 0;
      for (int i = 0; i < valueInBinary.Length; i++)
      {
        if (i%5 == 0)
        {
          numberOfSkips++;
        }
        else
        {
          if (valueInBinary[i] == '1')
          {
            double x = Math.Pow(2, totalDigitBits - i + numberOfSkips);
            if (Math.Floor(x) - x != 0)
            {
              throw new Exception();
            }
            if (Math.Ceiling(x) - x != 0)
            {
              throw new Exception();
            }
            Value += Convert.ToInt64(Math.Pow(2, totalDigitBits - i + numberOfSkips));
          }

          ;
        }
      }
    }
  }

  public class OperatorPacket : Packet
  {
    public int LengthType { get; set; }
    public List<Packet> SubPackets { get; set; }

    public OperatorPacket()
    {
      SubPackets = new List<Packet>();
    }

    public long CalculateValue()
    {
      long value = Type == 1 ? 1 : Type == 2 ? long.MaxValue : Type == 3 ? long.MinValue : 0;

      for (var index = 0; index < SubPackets.Count; index++)
      {
        Packet subPacket = SubPackets[index];
        OperatorPacket operatorPacket = subPacket as OperatorPacket;
        long valuePacket = 0;
        if (operatorPacket != null)
        {
          valuePacket = operatorPacket.CalculateValue();
        }
        else
        {
          valuePacket = ((LiteralPacket)subPacket).Value;
        }

        switch (Type)
        {
          case 0:
            value += valuePacket;
            break;
          case 1:
            value *= valuePacket;
            break;
          case 2:
            value = value > valuePacket ? valuePacket : value;
            break;
          case 3:
            value = value > valuePacket ? value : valuePacket;
            break;
          case 5:
            if (index == 0)
            {
              value = valuePacket;
            }
            else
            {
              value = value > valuePacket ? 1 : 0;
            }
            break;
          case 6:
            if (index == 0)
            {
              value = valuePacket;
            }
            else
            {
              value = value < valuePacket ? 1 : 0;
            }
            break;
          case 7:
            if (index == 0)
            {
              value = valuePacket;
            }
            else
            {
              value = value == valuePacket ? 1 : 0;
            }
            break;
        }
      }
      return value;
    }
  }

  public class OperatorType0Packet : OperatorPacket
  {
    public int LengthOfSubPacketsInBits { get; set; }

    public OperatorType0Packet()
    {
      LengthType = 0;
    }
  }
  
  public class OperatorType1Packet : OperatorPacket
  {
    public int NumberOfSubPackets { get; set; }

    public OperatorType1Packet()
    {
      LengthType = 1;
    }
  }
  
  class Program
  {
    static void Main(string[] args)
    {
      string[] lines = File.ReadAllLines("inputfile.txt");
      SolvePuzzle(lines[0]);
    }

    private static void SolvePuzzle(string line)
    {
      string binaryString =string.Join("",
        line.Select(c => Convert.ToString(Convert.ToInt32(c.ToString(), 16), 2).PadLeft(4, '0')).ToList());
      int packetStartIndex = 0;
      while (packetStartIndex < binaryString.Length-1)
      {
        (int nextPacketStartIndex, Packet createdPackets) result = ParsePackets(binaryString, packetStartIndex);
        Console.WriteLine(VersionCount(result.createdPackets));
        Console.WriteLine(((OperatorPacket)result.createdPackets).CalculateValue());
        packetStartIndex = result.nextPacketStartIndex;
      }
    }

    private static int VersionCount(Packet packet)
    {
      int sum = packet.Version;
      if (packet is OperatorPacket)
      {
        foreach (Packet subPacket in ((OperatorPacket)packet).SubPackets)
        {
          sum += VersionCount(subPacket);
        }
      }

      return sum;
    }

    private static (int nextPacketStartIndex, Packet createdPackets) ParsePackets(string binaryString, int packetStartIndex)
    {
      string versionString = binaryString.Substring(packetStartIndex, 3);
      int version = Convert.ToInt32(versionString, 2);
      string typeString = binaryString.Substring(packetStartIndex + 3, 3);
      int type = Convert.ToInt32(typeString, 2);
      if (type == 4) // literal
      {
        int groupBitIndex = packetStartIndex + 6;
        char leadingGroupBit = binaryString[groupBitIndex];
        while (leadingGroupBit == '1')
        {
          groupBitIndex += 5;
          leadingGroupBit = binaryString[groupBitIndex];
        }

        int literalLastBit = groupBitIndex + 5;
        LiteralPacket literal = new LiteralPacket(version, type,
          binaryString.Substring(packetStartIndex + 6, literalLastBit - (packetStartIndex + 6)));
        // packetStartIndex = literalLastBit + ((literalLastBit - packetStartIndex) % 4) + 1;
        packetStartIndex = literalLastBit;
        return new(packetStartIndex, literal);
      }

      char lengthTypeId = binaryString[packetStartIndex + 6];
      if (lengthTypeId == '1')
      {
        string numberOfSubPacketsString = binaryString.Substring(packetStartIndex + 7, 11);
        int numberOfSubPackets = Convert.ToInt32(numberOfSubPacketsString, 2);
        OperatorType1Packet packet = new OperatorType1Packet()
        {
          Type = type,
          LengthType = 1,
          Version = version,
          NumberOfSubPackets = numberOfSubPackets
        };
        int nextPacketStartIndex = packetStartIndex + 7 + 11;
        for (int i = 0; i < numberOfSubPackets; i++)
        {
          (int next, Packet createdPackets) subPacket = ParsePackets(binaryString, nextPacketStartIndex);
          packet.SubPackets.Add(subPacket.createdPackets);
          nextPacketStartIndex = subPacket.next;
        }
        // nextPacketStartIndex = nextPacketStartIndex + ((nextPacketStartIndex - packetStartIndex) % 4) + 1;
        // nextPacketStartIndex += 1;
        return new(nextPacketStartIndex, packet);
      }
      else
      {
        string lengthOfSubPacketsString = binaryString.Substring(packetStartIndex + 7, 15);
        int lengthOfSubPackets = Convert.ToInt32(lengthOfSubPacketsString, 2);
        OperatorType0Packet packet = new OperatorType0Packet()
        {
          Type = type,
          LengthType = 1,
          Version = version,
          LengthOfSubPacketsInBits = lengthOfSubPackets
        };
        int nextPacketStartIndex = packetStartIndex + 7 + 15;
        while(nextPacketStartIndex < packetStartIndex + 7 + 15 + lengthOfSubPackets)
        {
          (int next, Packet createdPackets) subPacket = ParsePackets(binaryString, nextPacketStartIndex);
          packet.SubPackets.Add(subPacket.createdPackets);
          nextPacketStartIndex = subPacket.next;
        }
        // nextPacketStartIndex = nextPacketStartIndex + ((nextPacketStartIndex - packetStartIndex) % 4) + 1;
        // nextPacketStartIndex += 1;
        return new(nextPacketStartIndex, packet);
      }
      
    }
  }
}