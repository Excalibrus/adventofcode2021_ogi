﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.IO;
using System.Linq;
using System.Security.Cryptography;

namespace puzzle_4
{
  public class BingoBoard
  {
    private List<List<int>> _verticalCombinations;
    private List<List<int>> _horizontalCombinations;
    
    public BingoBoard(List<string> lines)
    {
      if (lines.Count != 5)
      {
        throw new Exception("Bingo board must be 5x5.");
      }

      _horizontalCombinations = new List<List<int>>();
      foreach (string line in lines)
      {
        List<int> horizontalCombination = new List<int>();
        string[] lineNumbersStrings = line.Split(" ", StringSplitOptions.RemoveEmptyEntries);
        foreach (string lineNumberString in lineNumbersStrings)
        {
          horizontalCombination.Add(int.Parse(lineNumberString));
        }
        _horizontalCombinations.Add(horizontalCombination);
      }

      _verticalCombinations = new List<List<int>>();
      for (int i = 0; i < _horizontalCombinations.Count; i++)
      {
        List<int> verticalCombination = new List<int>();
        for (int j = 0; j < _horizontalCombinations.Count; j++)
        {
          verticalCombination.Add(_horizontalCombinations[j][i]);
        }
        _verticalCombinations.Add(verticalCombination);
      }
    }

    public bool NumberDraw(int numberDraw)
    {
      bool isBingo = false;
      foreach (List<int> combination in _horizontalCombinations)
      {
        combination.Remove(numberDraw);
        if (!combination.Any())
        {
          isBingo = true;
        }
      }
      
      foreach (List<int> combination in _verticalCombinations)
      {
        combination.Remove(numberDraw);
        if (!combination.Any())
        {
          isBingo = true;
        }
      }

      return isBingo;
    }

    public int GetRemainingSum()
    {
      int sum = 0;
      foreach (List<int> combination in _verticalCombinations)
      {
        int combinationSum = combination.Sum();
        sum += combinationSum;
      }

      return sum;
    }
  }
  
  class Program
  {
    static void Main(string[] args)
    {
      string[] lines = File.ReadAllLines("inputfile.txt");
      Part1(lines);
      Part2(lines);
    }

    private static void Part2(string[] lines)
    {
      var numberDraw = ParseDraw(lines[0]);
      var boards = ParseBoards(lines);
      foreach (int numberDrawn in numberDraw)
      {
        List<BingoBoard> boardsToRemove = new List<BingoBoard>();
        foreach (BingoBoard bingoBoard in boards)
        {
          bool isBingo = bingoBoard.NumberDraw(numberDrawn);
          if (isBingo)
          {
            boardsToRemove.Add(bingoBoard);
            if (boards.Count == 1)
            {
              Console.WriteLine(bingoBoard.GetRemainingSum()*numberDrawn);
            }
          }
        }
        foreach (BingoBoard bingoBoard in boardsToRemove)
        {
          boards.Remove(bingoBoard);
        }
      }
    }

    private static List<int> ParseDraw(string drawLine)
    {
      List<int> numberDraw = new List<int>();
      string[] draw = drawLine.Split(',');
      foreach (string numberString in draw)
      {
        numberDraw.Add(int.Parse(numberString));
      }

      return numberDraw;
    }

    private static void Part1(string[] lines)
    {
      var numberDraw = ParseDraw(lines[0]);
      var boards = ParseBoards(lines);
      foreach (int numberDrawn in numberDraw)
      {
        bool isBingoRound = false;
        foreach (BingoBoard bingoBoard in boards)
        {
          bool isBingo = bingoBoard.NumberDraw(numberDrawn);
          if (isBingo)
          {
            Console.WriteLine(bingoBoard.GetRemainingSum() * numberDrawn);
            isBingoRound = true;
          }
        }

        if (isBingoRound)
        {
          break;
        }
      }
    }

    private static List<BingoBoard> ParseBoards(string[] lines)
    {
      List<BingoBoard> boards = new List<BingoBoard>();
      int numberOfBoards = (lines.Length - 1) / 6;
      int lineIndex = 1;
      for (int i = 0; i < numberOfBoards; i++)
      {
        List<string> boardInput = new List<string>();
        lineIndex++;
        for (int j = 0; j < 5; j++)
        {
          boardInput.Add(lines[lineIndex]);
          lineIndex++;
        }

        BingoBoard board = new BingoBoard(boardInput);
        boards.Add(board);
      }

      return boards;
    }
  }
}