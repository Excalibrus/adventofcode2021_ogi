﻿using System;
using System.Collections.Generic;
using System.IO;
using Dijkstra.NET.Graph;
using Dijkstra.NET.ShortestPath;

namespace puzzle_15
{
  internal class Program
  {
    public static void Main(string[] args)
    {
      string[] lines = File.ReadAllLines("inputfile.txt");
      SolvePuzzle(lines, true);
      SolvePuzzle(lines, false);
    }

    private static void SolvePuzzle(string[] lines, bool isPart1)
    {
      if (!isPart1)
      {
        lines = MultiplyMatrix(lines);
      }
      
      Dictionary<string, uint> dict = new Dictionary<string, uint>();
      uint index = 1;
      Graph<int, string> graph = new Graph<int, string>();
      for (int lineIndex = 0; lineIndex < lines.Length; lineIndex++)
      {
        for (int charIndex = 0; charIndex < lines[lineIndex].Length; charIndex++)
        {
          graph.AddNode(0);
          dict.Add($"{lineIndex}_{charIndex}", index);
          index++;
        }
      }
      
      for (int lineIndex = 0; lineIndex < lines.Length; lineIndex++)
      {
        for (int charIndex = 0; charIndex < lines[lineIndex].Length; charIndex++)
        {
          if (lineIndex != 0)
          {
            graph.Connect(dict[$"{lineIndex}_{charIndex}"], dict[$"{lineIndex-1}_{charIndex}"], int.Parse(lines[lineIndex-1][charIndex].ToString()), String.Empty);      
          }
          if (lineIndex != lines.Length-1)
          {
            graph.Connect(dict[$"{lineIndex}_{charIndex}"], dict[$"{lineIndex+1}_{charIndex}"], int.Parse(lines[lineIndex+1][charIndex].ToString()), String.Empty);         
          }

          if (charIndex != 0)
          {
            graph.Connect(dict[$"{lineIndex}_{charIndex}"], dict[$"{lineIndex}_{charIndex-1}"], int.Parse(lines[lineIndex][charIndex-1].ToString()), String.Empty);         
          }
          if (charIndex != lines[lineIndex].Length-1)
          {
            graph.Connect(dict[$"{lineIndex}_{charIndex}"], dict[$"{lineIndex}_{charIndex+1}"], int.Parse(lines[lineIndex][charIndex+1].ToString()), String.Empty);         
          } 
          
        }
      }

      ShortestPathResult result = graph.Dijkstra(1, (uint)(lines.Length*lines.Length));
      Console.WriteLine((isPart1? "P1: " : "P2: ") + result.Distance);
    }

    private static string[] MultiplyMatrix(string[] lines)
    {
      string[] newLines = new string[lines.Length * 5];
      for (int i = 0; i < 5; i++)
      {
        for (int j = 0; j < 5; j++)
        {
          for (int lineIndex = 0; lineIndex < lines.Length; lineIndex++)
          {
            if (newLines[i * lines.Length + lineIndex] == null)
            {
              newLines[i * lines.Length + lineIndex] = String.Empty;
            }

            for (int charIndex = 0; charIndex < lines[lineIndex].Length; charIndex++)
            {
              int newValue = int.Parse(lines[lineIndex][charIndex].ToString()) + i + j;
              int numberOfOvershoots = newValue / 10;
              newValue = (newValue + numberOfOvershoots) % 10;
              if (newValue == 0)
              {
                newValue = 1;
              }

              newLines[i * lines.Length + lineIndex] += newValue;
            }
          }
        }
      }

      return newLines;
    }
  }
}