﻿using System;
using System.IO;

namespace puzzle_1
{
  class Program
  {
    static void Main(string[] args)
    {
      string[] readAllLines = File.ReadAllLines("inputfile.txt");
      
      Console.WriteLine($"Part1: {Part1(readAllLines)}");
      Console.WriteLine($"Part2: {Part2(readAllLines)}");
    }

    private static int Part1(string[] lines)
    {
      int? previousValue = null;
      int increaseCount = 0;
      for (int i = 0; i < lines.Length; i++)
      {
        var sonarDepth = int.Parse(lines[i]);
        if (!previousValue.HasValue)
        {
          previousValue = sonarDepth;
          continue;
        }
        if (sonarDepth > previousValue.Value)
        {
          increaseCount++;
        }
        previousValue = sonarDepth;
      }

      return increaseCount;
    }

    private static int Part2(string[] lines)
    {
      int? previousValue = null;
      int increaseCount = 0;
      for (int i = 0; i < lines.Length; i++)
      {
        if (i+2 >= lines.Length)
        {
          break;
        }
        var sonarDepth1 = int.Parse(lines[i]);
        var sonarDepth2 = int.Parse(lines[i+1]);
        var sonarDepth3 = int.Parse(lines[i+2]);
        var sonarDepth = sonarDepth1 + sonarDepth2 + sonarDepth3;
        if (!previousValue.HasValue)
        {
          previousValue = sonarDepth;
          continue;
        }
        if (sonarDepth > previousValue.Value)
        {
          increaseCount++;
        }
        previousValue = sonarDepth;
      }

      return increaseCount;
    }
  }
}