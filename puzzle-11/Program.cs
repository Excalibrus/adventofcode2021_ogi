﻿using System;
using System.IO;

namespace puzzle_11
{
  class Program
  {
    static void Main(string[] args)
    {
      string[] lines = File.ReadAllLines("inputfile.txt");
      SolvePuzzle(lines);
    }

    private static void SolvePuzzle(string[] lines)
    {
      var matrix = ParseEnergy(lines);
      bool part1Done = false;
      bool part2Done = false;
      int part1Result = 0;
      int part2Result = 0;
      int currentStepIndex = 1;
      while (!part1Done || !part2Done)
      {
        for (int i = 0; i < matrix.GetLength(0); i++)
        {
          for (int j = 0; j < matrix.GetLength(1); j++)
          {
            if (matrix[i,j] != -1)
            {
              matrix[i, j]++;
            }
            if (matrix[i,j] > 9)
            {
              matrix[i, j] = -1;
              IncreaseNeighbours(i, j, matrix);
            }
          }
        }

        int flashesInStep = 0;
        for (int i = 0; i < matrix.GetLength(0); i++)
        {
          for (int j = 0; j < matrix.GetLength(1); j++)
          {
            if (matrix[i,j] == -1)
            {
              flashesInStep++;
              matrix[i, j] = 0;
            }
          }
        }
        if (!part1Done)
        {
          part1Result += flashesInStep;
        }
        if (currentStepIndex == 100)
        {
          part1Done = true;
        }
        
        if (!part2Done && flashesInStep == 100)
        {
          part2Done = true;
          part2Result = currentStepIndex;
        }
        currentStepIndex++;
      }
      Console.WriteLine($"P1: {part1Result}");
      Console.WriteLine($"P2: {part2Result}");
    }

    private static void IncreaseNeighbours(int i, int j, int[,] matrix)
    {
      int leftMargin = i == 0 ? 0 : -1;
      int rightMargin = i == matrix.GetLength(0) - 1 ? 0 : 1;
      int topMargin = j == 0 ? 0 : -1;
      int bottomMargin = j == matrix.GetLength(1) - 1 ? 0 : 1;
      for (int k = leftMargin; k <= rightMargin; k++)
      {
        for (int l = topMargin; l <= bottomMargin; l++)
        {
          int firstIndex = i+k;
          int secondIndex = j+l;
          if (matrix[firstIndex, secondIndex] != -1)
          {
            matrix[firstIndex, secondIndex]++;
            if (matrix[firstIndex, secondIndex] > 9)
            {
              matrix[firstIndex, secondIndex] = -1;
              IncreaseNeighbours(firstIndex, secondIndex, matrix);
            }
          }
        }
      }
    } 

    private static int[,] ParseEnergy(string[] lines)
    {
      int[,] matrix = new int[10, 10];
      for (var i = 0; i < lines.Length; i++)
      {
        char[] line = lines[i].ToCharArray();
        for (var j = 0; j < line.Length; j++)
        {
          char octopusEnergy = line[j];
          matrix[i, j] = int.Parse(octopusEnergy.ToString());
        }
      }

      return matrix;
    }
  }
}