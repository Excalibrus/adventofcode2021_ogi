﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace puzzle_6
{
  public class LanternFish
  {
    public int Timer { get; set; }
    public LanternFish(int initialTimer)
    {
      Timer = initialTimer;
    }

    public LanternFish()
    {
      Timer = 8;
    }

    public LanternFish UpdateTimerAndProduceOffspringIfNeeded()
    {
      Timer--;
      if (Timer < 0)
      {
        Timer = 6;
        return new LanternFish();
      }

      return null;
    }
  }

  public class LanternFishAggregate
  {
    public int Timer { get; set; }
    public long NumberOfFishes { get; set; }

    public LanternFishAggregate(int initialTimer)
    {
      Timer = initialTimer;
      NumberOfFishes = 1;
    }
    
    public LanternFishAggregate(long numberOfFishes, int initialTimer)
    {
      Timer = initialTimer;
      NumberOfFishes = numberOfFishes;
    }
    
    public LanternFishAggregate UpdateTimerAndProduceOffspringIfNeeded()
    {
      Timer--;
      if (Timer < 0)
      {
        Timer = 6;
        return new LanternFishAggregate(NumberOfFishes, 8);
      }

      return null;
    }
  }
  
  class Program
  {
    static void Main(string[] args)
    {
      string[] lines = File.ReadAllLines("inputfile.txt");
      Part1(lines[0]);
      Part2(lines[0]);
    }

    private static void Part2(string line)
    {
      LanternFishAggregate[] fishes = new LanternFishAggregate[9];
      string[] agesString = line.Split(",",StringSplitOptions.RemoveEmptyEntries);
      int[] ages = new int[9];
      foreach (string ageString in agesString)
      {
        int age = int.Parse(ageString);
        ages[age]++;
      }

      for (var index = 0; index < ages.Length; index++)
      {
        fishes[index] = new LanternFishAggregate(ages[index], index); 
      }

      
      for (int i = 0; i < 256; i++)
      {
        LanternFishAggregate[] newFishes = new LanternFishAggregate[9];
        foreach (LanternFishAggregate fishAggregate in fishes)
        {
          LanternFishAggregate newFish = fishAggregate.UpdateTimerAndProduceOffspringIfNeeded();
          if (newFish != null)
          {
            if (newFishes[8] != null)
            {
              newFishes[8].NumberOfFishes += newFish.NumberOfFishes;
            }
            else
            {
              newFishes[8] = newFish;
            }
          }

          if (newFishes[fishAggregate.Timer] != null)
          {
            newFishes[fishAggregate.Timer].NumberOfFishes += fishAggregate.NumberOfFishes;
          }
          else
          {
            newFishes[fishAggregate.Timer] = fishAggregate;
          }
        }

        fishes = newFishes;
      }

      Console.WriteLine(fishes.Sum(x => x.NumberOfFishes));
    }

    public static void Part1(string line)
    {
      List<LanternFish> fishes = new List<LanternFish>();
      string[] ages = line.Split(",",StringSplitOptions.RemoveEmptyEntries);
      foreach (string ageString in ages)
      {
        int age = int.Parse(ageString);
        fishes.Add(new LanternFish(age));        
      }

      for (int i = 0; i < 80; i++)
      {
        ConcurrentBag<LanternFish> newFishes = new ConcurrentBag<LanternFish>();
        Parallel.ForEach(fishes, fish =>
        {
          LanternFish newFish = fish.UpdateTimerAndProduceOffspringIfNeeded();
          if (newFish != null)
          {
            newFishes.Add(newFish);
          }
        });
        
        fishes.AddRange(newFishes);
      }

      Console.WriteLine(fishes.Count);
    }
  }
}