﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace puzzle_14
{
  class Program
  {
    static void Main(string[] args)
    {
      string[] lines = File.ReadAllLines("inputfile.txt");
      SolvePuzzle(lines,10, "P1");
      SolvePuzzle(lines,40, "P2");
    }
    
    private static void SolvePuzzle(string[] lines, int numberOfSteps, string partName)
    {
      string problem = lines[0];
      Dictionary<string, string> dictionary = new Dictionary<string, string>();
      ConcurrentDictionary<string, Dictionary<char, long>> dictionaryStats = new ConcurrentDictionary<string, Dictionary<char, long>>();
      for (int i = 2; i < lines.Length; i++)
      {
        string line = lines[i];
        string[] parts = line.Split(" -> ", StringSplitOptions.RemoveEmptyEntries);
        dictionary.Add(parts[0], parts[1]);
      }

      Parallel.ForEach(dictionary, keyValuePair =>
      {
        // Console.WriteLine($"Calculating stats for {keyValuePair.Key}");
        var newValue = BuildPuzzleString(numberOfSteps/2, keyValuePair.Key, dictionary);
        Dictionary<char, long> stats = new Dictionary<char, long>();
        for (int i = 0; i < newValue.Length; i++)
        {
          if (!stats.ContainsKey(newValue[i]))
          {
            stats.Add(newValue[i],0);
          }
          stats[newValue[i]]++;
        }
        dictionaryStats[keyValuePair.Key] = stats;
      });

      StringBuilder newFinalValue = BuildPuzzleString(numberOfSteps / 2, problem, dictionary);
      
      Dictionary<char, long> finalStats = new Dictionary<char, long>();
      
      for (int i = 0; i < newFinalValue.Length-1; i++)
      {
        string lookupValue = newFinalValue[i].ToString() + newFinalValue[i+1];
          
        if (dictionaryStats.ContainsKey(lookupValue))
        {
          foreach (KeyValuePair<char,long> existingStats in dictionaryStats[lookupValue])
          {
            if (!finalStats.ContainsKey(existingStats.Key))
            {
              finalStats[existingStats.Key] = 0;
            }
            finalStats[existingStats.Key] += existingStats.Value;
          }

          if (i+1 != newFinalValue.Length-1)
          {
            finalStats[newFinalValue[i + 1]]--;
          }
        }
      }

      long mostCommon = finalStats.Values.Max();
      long leastCommon = finalStats.Values.Min();
      Console.WriteLine($"{partName} {mostCommon - leastCommon}");
    }

    private static StringBuilder BuildPuzzleString(int numberOfSteps, string startingString, Dictionary<string, string> dictionary)
    {
      StringBuilder newValue = new StringBuilder();
      string oldValue = startingString;
      for (int step = 0; step < numberOfSteps; step++)
      {
        newValue = new StringBuilder(oldValue.Length * 2);
        for (int i = 0; i < oldValue.Length - 1; i++)
        {
          string lookupValue = oldValue[i].ToString() + oldValue[i + 1];

          if (dictionary.ContainsKey(lookupValue))
          {
            if (i == 0)
            {
              newValue.Append(lookupValue[0]);
            }

            newValue.Append(dictionary[lookupValue] + lookupValue[1]);
          }
        }

        oldValue = newValue.ToString();
      }

      return newValue;
    }
  }
}