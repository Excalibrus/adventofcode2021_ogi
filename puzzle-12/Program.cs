﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;

namespace puzzle_12
{
  class Program
  {
    static void Main(string[] args)
    {
      string[] lines = File.ReadAllLines("inputfile.txt");
      SolvePuzzle(lines);
    }

    private static void SolvePuzzle(string[] lines)
    {
      List<Cave> caves = new List<Cave>();
      foreach (string line in lines)
      {
        string[] neighbourCaves = line.Split("-", StringSplitOptions.RemoveEmptyEntries);
        Cave cave1 = caves.FirstOrDefault(x => x.Name == neighbourCaves[0]);
        Cave cave2 = caves.FirstOrDefault(x => x.Name == neighbourCaves[1]);
        if (cave1 == null)
        {
          cave1 = new Cave(neighbourCaves[0]);
          caves.Add(cave1);
        }

        if (cave2 == null)
        {
          cave2 = new Cave(neighbourCaves[1]);
          caves.Add(cave2);
        }
        
        cave1.Neighbours.Add(cave2);
        cave2.Neighbours.Add(cave1);
      }

      Cave cave = caves.First(x => x.Name == "start");
      List<Cave> visitedCaves = new List<Cave>();
      List<Cave> cavePath = new List<Cave>();
      string path = "start";
      // while (cave != null && !visitedCaves.Contains(cave) || !cave.IsSmallCave)
      // {
      //   path = path + cave.Name + ",";
      //   visitedCaves.Add(cave);
      //   cavePath.Add(cave);
      //   cave = cave.Neighbours.FirstOrDefault(x => !visitedCaves.Contains(x) || !x.IsSmallCave);
      //   if (cave.Name == "end")
      //   {
      //     path = path + cave.Name;
      //     break;
      //   }
      // }
      List<string> paths = Rec(path, cave);
      paths = paths.Where(x => x.EndsWith("end")).ToList();
      Console.WriteLine(paths.Count);

      List<string> p2 = new List<string>();
      List<string> paths2 = Rec2(path, cave);
      paths2 = paths2.Where(x => x.EndsWith("end")).ToList();
      foreach (string path2 in paths2)
      {
        string[] strings = path2.Split(",");
        
        int orderedEnumerable = strings
          .GroupBy(str => str)
          .Select(group => new { 
            Name = @group.Key, 
            Count = Enumerable.Count<string>(@group) 
          })
          .Count(x => x.Name[0] == x.Name.ToLower()[0] && x.Count > 1);

        if (orderedEnumerable <= 1)
        {
          p2.Add(path2);
        }
      }
      Console.WriteLine(p2.Count);
    }

    private static List<string> Rec(string donePath, Cave cave)
    {
      List<string> paths = new List<string>();
      foreach (Cave neighbour in cave.Neighbours)
      {
        if (!donePath.Contains(neighbour.Name) || !neighbour.IsSmallCave)
        {
          string newPath = donePath + "," + neighbour.Name;
          paths.Add(newPath);
          if (neighbour.Name != "end")
          {
            paths.AddRange(Rec(newPath, neighbour));  
          }
        }   
      }

      return paths;
    }
    
    private static List<string> Rec2(string donePath, Cave cave)
    {
      List<string> paths = new List<string>();
      string[] strings = donePath.Split(",");
        
      int orderedEnumerable = strings
        .GroupBy(str => str)
        .Select(group => new { 
          Name = @group.Key, 
          Count = Enumerable.Count<string>(@group) 
        })
        .Count(x => x.Name[0] == x.Name.ToLower()[0] && x.Count > 1);
      
      foreach (Cave neighbour in cave.Neighbours)
      {
        if (neighbour.Name != "star1t" && (donePath.Split(",").Count(x => x == neighbour.Name) < 1 || (donePath.Split(",").Count(x => x == neighbour.Name) < 2 && orderedEnumerable == 0) || !neighbour.IsSmallCave))
        {
          string newPath = donePath + "," + neighbour.Name;
          paths.Add(newPath);
          if (neighbour.Name != "end")
          {
            paths.AddRange(Rec2(newPath, neighbour));  
          }
        }   
      }

      return paths;
    }
  }

  public class Cave
  {
    public bool IsSmallCave { get; set; }
    public string Name { get; set; }
    public List<Cave> Neighbours { get; set; }

    public Cave(string name)
    {
      Name = name;
      if (name[0] == name.ToLower()[0])
      {
        IsSmallCave = true;
      }

      Neighbours = new List<Cave>();
    }
  }
}