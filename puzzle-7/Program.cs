﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

namespace puzzle_7
{
  class Program
  {
    static void Main(string[] args)
    {
      string[] lines = File.ReadAllLines("inputfile.txt");
      string[] crabPositionStrings = lines[0].Split(",", StringSplitOptions.RemoveEmptyEntries);
      List<int> crabPositions = new List<int>();
      foreach (string crapPositionString in crabPositionStrings)
      {
        int i = int.Parse(crapPositionString);
        crabPositions.Add(i);
      }
      Part1(crabPositions);
      Part2(lines);
    }

    private static void Part2(string[] lines)
    {
      
    }

    private static void Part1(List<int> lines)
    {
      int from = lines.Min();
      int to = lines.Max();
      int minConsumption = int.MaxValue;
      for (int i = from; i <= to; i++)
      {
        int fuelUsed = 0;
        foreach (int position in lines)
        {
          int sumOfNumbers = Math.Abs(position - i);
          for (int j = 1; j <= sumOfNumbers; j++)
          {
            fuelUsed += j;
          }
        }

        if (fuelUsed < minConsumption)
        {
          minConsumption = fuelUsed;
        }
      }
      Console.WriteLine(minConsumption);
    }
  }
}