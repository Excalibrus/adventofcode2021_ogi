﻿using System;
using System.IO;

namespace puzzle_2
{
  class Program
  {
    static void Main(string[] args)
    {
      string[] readAllLines = File.ReadAllLines("inputfile.txt");

      Part1(readAllLines);
      Part2(readAllLines);
    }

    private static void Part2(string[] readAllLines)
    {
      int aim = 0;
      int horizontal = 0;
      int depth = 0;
      
      foreach (string move in readAllLines)
      {
        string[] parts = move.Split(" ");
        int moveValue = int.Parse(parts[1]);
        switch (parts[0].ToLowerInvariant())
        {
          case "forward":
            horizontal += moveValue;
            depth += aim * moveValue;
            break;
          case "up":
            aim -= moveValue;
            break;
          case "down":
            aim += moveValue;
            break;
          default:
            throw new Exception("unknown");
        }
      }

      Console.WriteLine($"H: {horizontal}, V: {depth}, H*V: {horizontal * depth}");
    }

    private static void Part1(string[] readAllLines)
    {
      int verticalPosition = 0;
      int horizontalPosition = 0;
      foreach (string move in readAllLines)
      {
        string[] parts = move.Split(" ");
        int moveValue = int.Parse(parts[1]);
        switch (parts[0].ToLowerInvariant())
        {
          case "forward":
            horizontalPosition += moveValue;
            break;
          case "up":
            verticalPosition -= moveValue;
            break;
          case "down":
            verticalPosition += moveValue;
            break;
          default:
            throw new Exception("unknown");
        }
      }

      Console.WriteLine($"H: {horizontalPosition}, V: {verticalPosition}, H*V: {horizontalPosition * verticalPosition}");
    }
  }
}