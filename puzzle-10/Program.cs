﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace puzzle_10
{
  

  public class Chunk
  {
    public static string AddedStrings = "";    
    
    public char OpenCharacter { get; set; }
    public char CloseCharacter { get; set; }
    public List<Chunk> Children { get; set; }
    public char[] RemainingCharacters { get; set; }

    public Chunk(char[] characters)
    {
      Children = new List<Chunk>();
      OpenCharacter = characters[0];
      if (OpenCharacter == '}' ||
          OpenCharacter == ')' ||
          OpenCharacter == ']' ||
          OpenCharacter == '>'
        )
      {
        throw new Exception(OpenCharacter.ToString());
      }
      if (characters.Length > 1 && 
          (OpenCharacter == '{' && characters[1] == '}' || 
           OpenCharacter == '(' && characters[1] == ')' ||
           OpenCharacter == '[' && characters[1] == ']' ||
           OpenCharacter == '<' && characters[1] == '>'))
      {
        CloseCharacter = characters[1];
        RemainingCharacters = characters.Skip(2).Take(characters.Length - 2).ToArray();
        return;
      }
      else if (OpenCharacter == '}' ||
          OpenCharacter == ')' ||
          OpenCharacter == ']' ||
          OpenCharacter == '>')
      {
        throw new Exception(OpenCharacter.ToString());
      }
      else
      {
        if (characters.Length>1)
        {
          Chunk childChunk = new Chunk(characters.Skip(1).Take(characters.Length - 1).ToArray());
          Children.Add(childChunk);
          while (childChunk.RemainingCharacters != null && childChunk.RemainingCharacters.Any() &&
                 (childChunk.RemainingCharacters[0] == '{' ||
                  childChunk.RemainingCharacters[0] == '(' ||
                  childChunk.RemainingCharacters[0] == '[' ||
                  childChunk.RemainingCharacters[0] == '<'))
          {
            childChunk = new Chunk(childChunk.RemainingCharacters);
            Children.Add(childChunk);
          }

          if (childChunk.RemainingCharacters != null && childChunk.RemainingCharacters.Any() &&
              (OpenCharacter == '{' && childChunk.RemainingCharacters[0] != '}' || 
               OpenCharacter == '(' && childChunk.RemainingCharacters[0] != ')' || 
               OpenCharacter == '[' && childChunk.RemainingCharacters[0] != ']' || 
               OpenCharacter == '<' && childChunk.RemainingCharacters[0] != '>'))
          {
            throw new Exception(childChunk.RemainingCharacters[0].ToString());
          }

          if (childChunk.RemainingCharacters != null && childChunk.RemainingCharacters.Any())
          {
            CloseCharacter = childChunk.RemainingCharacters[0];
            RemainingCharacters = childChunk.RemainingCharacters.Skip(1).Take(characters.Length - 1).ToArray();
            return;
          }
          if (childChunk.RemainingCharacters == null || !childChunk.RemainingCharacters.Any())
          {
            switch (OpenCharacter)
            {
              case '{':
                AddedStrings += "}";
                break;
              case '(':
                AddedStrings += ")";
                break;
              case '<':
                AddedStrings += ">";
                break;
              case '[':
                AddedStrings += "]";
                break;
            }
            return;
          }
        }
        else
        {
          switch (OpenCharacter)
          {
            case '{':
              AddedStrings += "}";
              break;
            case '(':
              AddedStrings += ")";
              break;
            case '<':
              AddedStrings += ">";
              break;
            case '[':
              AddedStrings += "]";
              break;
          }
          return;
        }

        
      }
    }
  }
  
  class Program
  {
    static void Main(string[] args)
    {
      string[] lines = File.ReadAllLines("inputfile.txt");
      Part1(lines);
    }

    private static void Part1(string[] lines)
    {
      long sum = 0;
      List<long> scores = new List<long>();
      foreach (string line in lines)
      {
        try
        {
          Chunk.AddedStrings = "";
          Chunk ch = new Chunk(line.ToCharArray());
          while (ch.RemainingCharacters != null && ch.RemainingCharacters.Any())
          {
            ch = new Chunk(ch.RemainingCharacters);
          }
          char[] charArray = Chunk.AddedStrings.ToCharArray();
          long score = 0;
          foreach (char missingChar in charArray)
          {
            switch (missingChar)
            {
              case '}':
                score = score * 5 + 3;
                break;
              case ')':
                score = score * 5 + 1;
                break;
              case '>':
                score = score * 5 + 4;
                break;
              case ']':
                score = score * 5 + 2;
                break;
            }
          }
          scores.Add(score);
        }
        catch (Exception e)
        {
          if (e.Message == ")")
          {
            sum += 3;
          }
          if (e.Message == "]")
          {
            sum += 57;
          }
          if (e.Message == "}")
          {
            sum += 1197;
          }
          if (e.Message == ">")
          {
            sum += 25137;
          }
        }
      }
      Console.WriteLine(sum);
      scores.Sort();
      Console.WriteLine(scores[(int)Math.Floor(scores.Count/2.0)]);
    }
  }
}