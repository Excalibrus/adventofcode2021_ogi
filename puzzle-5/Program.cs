﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace puzzle_5
{
  public class Line
  {
    public int MaxX { get; set; }
    public int MaxY { get; set; }
    public bool IsDiagonal { get; set; }
    private List<Tuple<int, int>> _lineEnds;
    public Line(string lineString)
    {
      _lineEnds = new List<Tuple<int, int>>();
      string[] lineEnds = lineString.Split("->");
      foreach (string lineEnd in lineEnds)
      {
        string trimmed = lineEnd.Trim();
        string[] pointCoordinateStrings = trimmed.Split(",", StringSplitOptions.RemoveEmptyEntries);
        int x = int.Parse(pointCoordinateStrings[0]);
        int y = int.Parse(pointCoordinateStrings[1]);
        _lineEnds.Add(new Tuple<int, int>(x, y));
      }

      MaxX = _lineEnds.Max(x => x.Item1);
      MaxY = _lineEnds.Max(x => x.Item2);
      IsDiagonal = _lineEnds[0].Item1 != _lineEnds[1].Item1 && _lineEnds[0].Item2 != _lineEnds[1].Item2;
    }

    public List<Tuple<int, int>> GetLinePoints()
    {
      List<Tuple<int, int>> returnList = new List<Tuple<int, int>>();

      int xCoordinateAdd = _lineEnds[0].Item1 < _lineEnds[1].Item1 ? 1 :
        _lineEnds[0].Item1 == _lineEnds[1].Item1 ? 0 : -1;
      int yCoordinateAdd = _lineEnds[0].Item2 < _lineEnds[1].Item2 ? 1 :
        _lineEnds[0].Item2 == _lineEnds[1].Item2 ? 0 : -1;
      int xCoordinate = _lineEnds[0].Item1;
      int yCoordinate = _lineEnds[0].Item2;
      while (xCoordinate != _lineEnds[1].Item1 || yCoordinate != _lineEnds[1].Item2)
      {
        returnList.Add(new Tuple<int, int>(xCoordinate, yCoordinate));
        xCoordinate = xCoordinate + xCoordinateAdd;
        yCoordinate = yCoordinate + yCoordinateAdd;
      }
      returnList.Add(new Tuple<int, int>(xCoordinate, yCoordinate));
      return returnList;
    }
  }
  
  class Program
  {
    static void Main(string[] args)
    {
      string[] lines = File.ReadAllLines("inputfile.txt");
      SolvePuzzle(lines, false); // 8060
      SolvePuzzle(lines, true); // 21577
    }

    private static void SolvePuzzle(string[] lineStrings, bool includeDiagonalLines)
    {
      List<Line> lines = new List<Line>(); 
      foreach (string lineString in lineStrings)
      {
        Line line = new Line(lineString);
        if (includeDiagonalLines || !line.IsDiagonal)
        {
         lines.Add(line); 
        }
      }

      int xDimension = lines.Max(x => x.MaxX) + 1;
      int yDimension = lines.Max(x => x.MaxY) + 1;

      int[,] coordinateSystem = new int[xDimension, yDimension];
      foreach (Line line in lines)
      {
        List<Tuple<int,int>> linePoints = line.GetLinePoints();
        foreach (Tuple<int,int> linePoint in linePoints)
        {
          coordinateSystem[linePoint.Item1, linePoint.Item2]++;
        }
      }

      int overlappingPoints = 0;
      for (int x = 0; x < xDimension; x++)
      {
        for (int y = 0; y < yDimension; y++)
        {
          if (coordinateSystem[x,y] >= 2)
          {
            overlappingPoints++;
          }
        }
      }
      Console.WriteLine($"{overlappingPoints}");
    }
  }
}