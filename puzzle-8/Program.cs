﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace puzzle_8
{
  class Program
  {
    static void Main(string[] args)
    {
      string[] lines = File.ReadAllLines("inputfile.txt");
      Part1(lines);
      Part2(lines);
    }

    public static void Part1(string[] lines)
    {
      int occurences = 0;
      foreach (string line in lines)
      {
        string[] strings = line.Split("|")[1].Split(" ", StringSplitOptions.RemoveEmptyEntries);
        occurences += strings.Count(x => x.Length == 2 || x.Length == 3 || x.Length == 4 || x.Length == 7);
      }

      Console.WriteLine(occurences);
    }

    public static void Part2(string[] lines)
    {
      int sum = 0;
      foreach (string line in lines)
      {
        Dictionary<int, string> numberStrings = new Dictionary<int, string>();
        string[] insAndOuts = line.Split("|");
        string[] inputs = insAndOuts[0].Split(" ", StringSplitOptions.RemoveEmptyEntries);
        string[] outputs = insAndOuts[1].Split(" ", StringSplitOptions.RemoveEmptyEntries);
        List<string> learningPool = inputs.Concat(outputs).ToList();
        numberStrings[1] = learningPool.First(x => x.Length == 2);
        numberStrings[7] = learningPool.First(x => x.Length == 3);
        numberStrings[4] = learningPool.First(x => x.Length == 4);
        numberStrings[8] = learningPool.First(x => x.Length == 7);
        numberStrings[3] = learningPool.First(x =>
          x.Length == 5 && 
          x.ToCharArray().Except(numberStrings[7].ToCharArray()).ToList().Count == 2);
        numberStrings[6] = learningPool.First(x =>
          x.Length == 6 && 
          x.ToCharArray().Except(numberStrings[1].ToCharArray()).ToList().Count == 5);
        numberStrings[0] = learningPool.First(x =>
          x.Length == 6 && 
          x.ToCharArray().Except(numberStrings[3].ToCharArray().Except(numberStrings[7].ToCharArray())).ToList().Count == 5);
        numberStrings[9] = learningPool.First(x =>
          x.Length == 6 && 
          x.ToCharArray().Intersect(numberStrings[6].ToCharArray()).ToList().Count != 6 &&
          x.ToCharArray().Intersect(numberStrings[0].ToCharArray()).ToList().Count != 6);
        numberStrings[2] = learningPool.First(x =>
          x.Length == 5 && 
          x.ToCharArray().Contains(numberStrings[0].ToCharArray().Except(numberStrings[9].ToCharArray()).ToList()[0]));
        numberStrings[5] = learningPool.First(x =>
          x.Length == 5 && 
          x.ToCharArray().Intersect(numberStrings[2].ToCharArray()).ToList().Count != 5 &&
          x.ToCharArray().Intersect(numberStrings[3].ToCharArray()).ToList().Count != 5);
        string outputNumberString = "";
        foreach (string output in outputs)
        {
          char[] outputCharArray = output.ToCharArray();
          for (int i = 0; i < 10; i++)
          {
            if (numberStrings[i].Length == output.Length &&
                numberStrings[i].ToCharArray().Intersect(outputCharArray).ToList().Count == output.Length)
            {
              outputNumberString = outputNumberString + i;
            }
          }
        }

        sum += int.Parse(outputNumberString);
      }

      Console.WriteLine(sum);
    }
  }
}