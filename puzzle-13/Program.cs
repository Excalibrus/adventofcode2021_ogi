﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace puzzle_13
{
  class Program
  {
    static void Main(string[] args)
    {
      string[] lines = File.ReadAllLines("inputfile.txt");
      SolvePuzzle(lines);
    }

    private static void SolvePuzzle(string[] lines)
    {
      List<(int x, int y)> coordinates = new List<(int x, int y)>();
      List<(int x, int y)> folds = new List<(int x, int y)>();
      foreach (string line in lines)
      {
        if (string.IsNullOrWhiteSpace(line))
        {
          continue;
        }

        if (line.StartsWith("fold"))
        {
          bool isXFold = line[11] == 'y';
          string foldValueString = line.Remove(0, 13);
          int foldValue = int.Parse(foldValueString);
          if (isXFold)
          {
            folds.Add(new (foldValue, 0));
          }
          else
          {
            folds.Add(new (0, foldValue));
          }
        }
        else
        {
          string[] coordinateStrings = line.Split(",", StringSplitOptions.RemoveEmptyEntries);
          int x = int.Parse(coordinateStrings[1]);
          int y = int.Parse(coordinateStrings[0]);
          coordinates.Add(new (x, y));  
        }
        
      }
      int xDim = coordinates.Max(x => x.x) + 1;
      int yDim = coordinates.Max(x => x.y) + 1;

      string[,] matrix = new string[xDim, yDim];
      foreach ((int x, int y) coordinate in coordinates)
      {
        matrix[coordinate.x, coordinate.y] = "#";
      }

      int index = 0;
      int numberOfLojtrca = 0;
      foreach ((int x, int y) fold in folds)
      {
        numberOfLojtrca = 0;
        string[,] afterFold;
        if (fold.x != 0)
        {
          afterFold = new string[(matrix.GetLength(0) - 1) / 2, matrix.GetLength(1)];
          for (int i = 0; i < matrix.GetLength(0); i++)
          {
            for (int j = 0; j < matrix.GetLength(1); j++)
            {
              if (i < fold.x)
              {
                afterFold[i, j] = matrix[i, j];
              }
              else if(i>fold.x)
              {
                afterFold[fold.x - (i - fold.x), j] = afterFold[fold.x - (i - fold.x), j] != "#" ? matrix[i, j] : "#";
                if (afterFold[fold.x - (i - fold.x), j] == "#")
                {
                  numberOfLojtrca++;
                }
              }
            }
          }
        }
        else
        {
          afterFold = new string[matrix.GetLength(0), (matrix.GetLength(1)-1)/2];
          for (int i = 0; i < matrix.GetLength(0); i++)
          {
            for (int j = 0; j < matrix.GetLength(1); j++)
            {
              if (j < fold.y)
              {
                afterFold[i, j] = matrix[i, j];
              }
              else if(j>fold.y)
              {
                afterFold[i,fold.y - (j - fold.y)] = afterFold[i,fold.y - (j - fold.y)] != "#" ? matrix[i, j] : "#";
                if (afterFold[i,fold.y - (j - fold.y)] == "#")
                {
                  numberOfLojtrca++;
                }
              }
            }
          }
        }

        matrix = afterFold;
        if (index == 0)
        {
          Console.WriteLine($"P1: {numberOfLojtrca}");
        }

        index++;
      }
      
      for (int i = 0; i < matrix.GetLength(0); i++)
      {
        for (int j = 0; j < matrix.GetLength(1); j++)
        {
          Console.Write(matrix[i,j] == "#" ? "#" : ".");
        }
        Console.Write(Environment.NewLine);
      }
    }
  }
}