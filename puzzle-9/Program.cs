﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace puzzle_9
{
  class Program
  {
    static void Main(string[] args)
    {
      string[] lines = File.ReadAllLines("inputfile.txt");
      SolvePuzzle(lines);
    }

    private static void SolvePuzzle(string[] lines)
    {
      int[,] matrix = new int[lines.Length, lines[0].Length];
      for (var lineIndex = 0; lineIndex < lines.Length; lineIndex++)
      {
        string lineString = lines[lineIndex];
        for (int numberIndex = 0; numberIndex < lineString.Length; numberIndex++)
        {
          matrix[lineIndex, numberIndex] = int.Parse(lineString[numberIndex].ToString());
        }
      }

      int sum = 0;
      List<Tuple<int, int>> lowPoints = new List<Tuple<int, int>>();
      for (int i = 0; i < matrix.GetLength(0); i++)
      {
        for (int j = 0; j < matrix.GetLength(1); j++)
        {
          if ((i != 0 && matrix[i-1, j] > matrix[i, j] || i == 0) &&
              (i != matrix.GetLength(0)-1 && matrix[i+1,j] > matrix[i,j] || i == matrix.GetLength(0)-1) &&
              (j != 0 && matrix[i, j-1] > matrix[i, j] || j == 0) &&
              (j != matrix.GetLength(1)-1 && matrix[i,j+1] > matrix[i,j] || j == matrix.GetLength(1)-1))
          {
            sum += matrix[i, j] + 1;
            lowPoints.Add(new Tuple<int, int>(i, j));
          }
        }
      }
      Console.WriteLine($"P1: {sum}");
      Dictionary<Tuple<int, int>, int> lowPointBasinCount = new Dictionary<Tuple<int, int>, int>();
      foreach (Tuple<int,int> lowPoint in lowPoints)
      {
        List<Tuple<int, int>> basinPoints = new List<Tuple<int, int>>{lowPoint};
        
          
        for (int i = 0; i < basinPoints.Count; i++)
        {
          int x = basinPoints[i].Item1;
          int y = basinPoints[i].Item2;
          matrix[x, y] = -1;
          if (x != 0 && matrix[x-1, y] != 9 && matrix[x-1, y] != -1)
          {
            basinPoints.Add(new Tuple<int, int>(x-1, y));
            matrix[x - 1, y] = -1;
          }
          if (x < matrix.GetLength(0)-1 && matrix[x+1, y] != 9 && matrix[x+1, y] != -1)
          {
            basinPoints.Add(new Tuple<int, int>(x+1, y));
            matrix[x + 1, y] = -1;
          }
          if (y != 0 && matrix[x, y-1] != 9 && matrix[x, y-1] != -1)
          {
            basinPoints.Add(new Tuple<int, int>(x, y-1));
            matrix[x, y -1] = -1;
          }
          if (y < matrix.GetLength(1)-1 && matrix[x, y+1] != 9 && matrix[x, y+1] != -1)
          {
            basinPoints.Add(new Tuple<int, int>(x, y+1));
            matrix[x , y+ 1] = -1;
          }
        }
        lowPointBasinCount.Add(lowPoint, basinPoints.Count);
      }

      List<int> top3 = lowPointBasinCount.OrderByDescending(x => x.Value).Take(3).Select(x => x.Value).ToList();
      
      Console.WriteLine($"P2: {top3[0]*top3[1]*top3[2]}");
    }
  }
}