﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace puzzle_3
{
  class Program
  {
    static void Main(string[] args)
    {
      string[] lines = File.ReadAllLines("inputfile.txt");
      Part1(lines);
      Part2(lines);
    }

    private static void Part1(string[] lines)
    {
      (int[] ones, int[] zeros) = CalcBitStats(lines.ToList());
      int gamma = 0;
      int epsilon = 0;
      for (int i = 0; i < ones.Length; i++)
      {
        gamma = gamma << 1;
        epsilon = epsilon << 1;
        if (ones[i] > zeros[i])
        {
          gamma |= 1;
        }
        else
        {
          epsilon |= 1;
        }
      }
      Console.WriteLine($"Gamma: {gamma}"); // 1869
      Console.WriteLine($"Epsilon: {epsilon}"); // 2226
    }

    private static void Part2(string[] lines)
    {
      List<string> oxygenLines = new List<string>(lines);
      List<string> co2Lines = new List<string>(lines);
      for (int i = 0; i < lines[0].Length; i++)
      {
        oxygenLines = FilterLinesBasedOnBitX(oxygenLines, i, true);
        co2Lines = FilterLinesBasedOnBitX(co2Lines, i, false);
      }
      Console.WriteLine("O: "+Convert.ToInt32(oxygenLines[0], 2)); // 1719
      Console.WriteLine("CO2: "+ Convert.ToInt32(co2Lines[0], 2)); // 2400
    }

    private static List<string> FilterLinesBasedOnBitX(List<string> allLines, int bitIndex, bool useMostCommonBit)
    {
      List<string> filteredLines = new List<string>();
      if (allLines.Count > 1)
      {
        var (ones, zeros) = CalcBitStats(allLines);
        char searchBitValue = useMostCommonBit ? '0' : '1';
        if (ones[bitIndex] >= zeros[bitIndex])
        {
          searchBitValue = useMostCommonBit ? '1': '0';
        }

        filteredLines.AddRange(allLines.Where(line => line[bitIndex] == searchBitValue));
      }

      return filteredLines.Any() ? filteredLines : new List<string>(allLines);
    }

    private static (int[] ones,int[] zeros) CalcBitStats(List<string> lines)
    {
      int[] ones = new int[lines[0].Length];
      int[] zeros = new int[lines[0].Length];
      foreach (string line in lines)
      {
        for (int i = 0; i < line.Length; i++)
        {
          int digit = int.Parse(line[i].ToString());
          if (digit == 0)
          {
            zeros[i]++;
          }
          else
          {
            ones[i]++;
          }
        }
      }

      return (ones, zeros);
    }
  }
}